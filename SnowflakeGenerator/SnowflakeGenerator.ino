#include <Wire.h>
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_GFX.h"
#include "Adafruit_DotStar.h"
#include "Snowflake.h"
#include "DotstarFader.h"

//Initilize the on board Neopixel as well as the LED Matrix
DotstarFader fader(1, INTERNAL_DS_DATA, INTERNAL_DS_CLK, DOTSTAR_BGR);
Adafruit_8x8matrix matrix = Adafruit_8x8matrix();

//Globals 
int randomNumber;
int counter;
int fallRate = 300;
unsigned long previousMillis = 0;
unsigned long previousMillisSnow = 0;

//Change the array size to change the amount of large or small snowflakes
Snowflake snowArray[5];
int snowArraySize = sizeof(snowArray) / sizeof(snowArray[0]);
//----------------

void setup() {
  Serial.begin(9600);
  //while(!Serial); //Needed to see Serial.print results in setup function
  randomSeed(analogRead(1));

  //Initialize the matrix
  matrix.begin(0x70);  // pass in the address
  matrix.setBrightness(1);
  matrix.clear();
  matrix.writeDisplay();

  //Initialize the snowflakes
  for (int i = 0; i < snowArraySize; i++) {
    snowArray[i].setMatrix( &matrix );
    snowArray[i].setYBuffer(-2);
  }
}

void loop() {
  dropSnow();
  updateSnow();
  fader.fadePixel();
}

void dropSnow() {
  unsigned long currentMillis = millis();
  int randSeconds = random(1100, 1500); 
  if ( currentMillis - previousMillis >= randSeconds ) {
    previousMillis = currentMillis;
    
    for (int i = 0; i < snowArraySize; i++) {
      //If the flake isn't currently falling, start it falling again. 
      //The order is kept by using the break statement below since only one flake
      //is set at a time. 
      if (!snowArray[i].isFalling) {
        snowArray[i].isFalling = true;
        break;
      }
    }
  }
}

void updateSnow() {

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillisSnow >= fallRate ) {
    previousMillisSnow = currentMillis;
    matrix.clear();

    for (int i = 0; i < snowArraySize; i++) {
      if (snowArray[i].isFalling == true) {
        snowArray[i].update();
      }
    }

    //Write all changes to the display
    matrix.writeDisplay();
  }
}
