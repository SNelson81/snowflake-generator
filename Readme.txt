README

PREREQUISITES: 
For this project, you will need an Adafruit 8x8 LED Matrix w/backpack, and a Trinket M0. You should easily be able to adapt this to other boards but the code was built for those two hardware components. If you do use a different board you'll likely need to change the dotstar pin, the analog pin, and the Adafruit libraries to match your board. 


If you do not already have them, you'll need the following Adafruit libraries: 

Adafruit_LEDBackpack
Adafruit_GFX
Adafruit_Dotstar 

You can find how to get these libraries from here: https://learn.adafruit.com/adafruit-all-about-arduino-libraries-install-use/arduino-libraries

Any libraries used (Adafruit libraries, Snowflake, and DotstarFader) will need to be copied into the libraries folder where your Arduino IDE looks for them. On Mac, it's usually Documents -> Arduino -> libraries. 

WIRING: 
The wiring from the LED Backpack to the Trinket M0 is as follows: 

Backpack: SCL -> TrinketM0: 2 //Clock
Backpack: SDA -> TrinketM0: 0 //Data
Backpack: GND -> TricketM0: GND //Ground 
Backpack: VCC -> TricketM0: USB //Power 

NOTE: Double check this incase the Trinket M0 changed since I bought mine!

IDE: 
Make sure you have the TrinketM0 board set up in the Arduino IDE, if you do not, follow this tutorial on how to install the Adafruit boards: https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino/arduino-ide-setup

LIBRARIES: 
There are two libraries, the Snowflake library, which draws the snowflakes on the led matrix, and the dotstar fader, which fakes the on board Neopixel. If your board doesn't have a Neopixel comment out the dotstar fader code. If you're using a board that isn't a Trinket M0, be sure to choose the correct pin for the dotstar fader. 

3D PRINTING: 
Included are the .obj files for both the stand and the snowflake. Put them in your favorite slicer (I use Cura, it's free) and print away. It's just big enough to fit the Trinket M0 so if you're using a different board you'll need to create a new base or adjust the current one.  