#ifndef Snowflake_h
#define Snowflake_h

#include "Adafruit_GFX.h"
#include "Adafruit_LEDBackpack.h"


typedef enum FlakeSize {smallFlake, largeFlake} FlakeSize;
typedef enum SnowType {smallSnow, largeSnow} SnowType;

class Snowflake: public Adafruit_LEDBackpack {
    
private:
    int _minRangeX = 1;
    int _maxRangeX = 7;
    int _minRangeY = 0;
    int _maxRangeY = 8;
    int _yBuffer = 0; //This determines how far off screen the flake starts
    Adafruit_GFX *_matrix;
    FlakeSize flakeSize = smallFlake;
    void drawSnowflake();
    void drawLargeFlake(); 
    void drawSmallFlake();
    
public:
    bool isFalling = false;
    int xPos = 0;
    int yPos = 0;
    SnowType snowType;
    Snowflake();
    void setMatrix(Adafruit_8x8matrix *matrix);
    void setYBuffer(int value);
    void update();
    void reset();
};

#endif
