#include "Snowflake.h"

Snowflake::Snowflake() {
    this->reset();
}

void Snowflake::setMatrix(Adafruit_8x8matrix *matrix) {
    _matrix = matrix;
}

void Snowflake::setYBuffer(int value) {
    _yBuffer = value;
}

void Snowflake::update() {
    
    if (isFalling == true) {
        int val = random(0,2);
        val == 0 ? xPos-- : xPos++;
        yPos++;
        
        //If the snowflake is going off the screen, nudge it back
        if (xPos < _minRangeX || xPos > _maxRangeX) {
            xPos < _minRangeX ? xPos += 1 : xPos -=1;
        }
        
        if (yPos > _maxRangeY) {
            this->reset();
        }
        
        drawSnowflake();
    }
}

void Snowflake::reset() {
    isFalling = false;
    xPos = random(_minRangeX, _maxRangeX);
    yPos = _yBuffer;
    
    int val = random(0, 9); //40% chance of a large flake
    val < 3 ? snowType = largeSnow : snowType = smallSnow;
}

void Snowflake::drawSnowflake() {
 
    switch (snowType) {
        case smallSnow: //Snow is a dot
            _matrix->drawPixel(xPos, yPos, LED_ON);
            break;
            
        case largeSnow: //Snow is an animated flake
            if (flakeSize == smallFlake) {
                this->drawSmallFlake();
                flakeSize = largeFlake;
            } else {
                this->drawLargeFlake();
                flakeSize = smallFlake;
            }
            break;
    }
}

// Draw the large snowflake image
void Snowflake::drawLargeFlake() {
  _matrix->drawPixel(xPos, yPos, LED_ON);
  _matrix->drawPixel(xPos - 1, yPos - 1, LED_ON);
  _matrix->drawPixel(xPos - 1, yPos + 1, LED_ON);
  _matrix->drawPixel(xPos + 1, yPos - 1, LED_ON);
  _matrix->drawPixel(xPos + 1, yPos + 1, LED_ON);
}

//Draw the small snowflake image
void Snowflake::drawSmallFlake() {
  _matrix->drawPixel(xPos, yPos, LED_ON);
  _matrix->drawPixel(xPos, yPos - 1, LED_ON);
  _matrix->drawPixel(xPos - 1, yPos, LED_ON);
  _matrix->drawPixel(xPos + 1, yPos, LED_ON);
  _matrix->drawPixel(xPos, yPos + 1, LED_ON);
}
