/*
 Scott Nelson - Dec 19 2017
 Very basic library to control fading of the Trinket M0
 DotStar.
 */

#include "DotstarFader.h"

void DotstarFader::fadePixel() {
    unsigned long currentMillis = millis();
    
    //If the interval is reached, change the brightness
    if (currentMillis - previousMillisFade >= fadeInterval) {
        previousMillisFade = currentMillis;
        brightness += fadeAmount;
        
        //If the cycle completes, reset the fade
        if (brightness <= 0 || brightness >= 255) {
            fadeAmount = - fadeAmount;
            
            //If brightness is back to zero, switch the states (and colors)
            if (brightness <= 0) {
                (state == 0) ? state = 1 : state = 0;
            }
        }
        
        //Draw the color based on the states, the second color equates to a nice ice-blue.
        (state == 0) ? setPixelColor(0, brightness, brightness, brightness) : setPixelColor(0, brightness / 21.25, brightness / 7.5, brightness);
        show();
    }
}
