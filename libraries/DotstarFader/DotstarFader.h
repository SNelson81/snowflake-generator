/*
 Scott Nelson - Dec 19 2017
 Very basic library to control fading of the Trinket M0
 DotStar.
*/

#ifndef DotstarFader_h
#define DotstarFader_h

#include "Adafruit_GFX.h"
#include "Adafruit_DotStar.h"

class DotstarFader: public Adafruit_DotStar {
    
private:
    //Fade timer control
    uint32_t state = 0; //0 for color1, 1 for color2
    uint32_t brightness = 0;
    uint32_t fadeAmount = 1; //Steps to fade by (lower numbers mean smoother transitions)
    const long fadeInterval = 15; //Milliseconds until next fade step
    unsigned long previousMillisFade = 0;
    
public:
    DotstarFader(uint16_t pixelNumber, uint8_t data, uint8_t clk, uint8_t order): Adafruit_DotStar(pixelNumber, data, clk, order) {
        begin();
    }
    
    void fadePixel();
};

#endif
